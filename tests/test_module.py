# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class StaffTestCase(ModuleTestCase):
    """Test module"""
    module = 'staff'
    extras = ['company_employee_team']


del ModuleTestCase
